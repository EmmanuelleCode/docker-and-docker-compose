from os import curdir
import re
from flask import Flask, render_template, request
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'colletion'

mysql = MySQL(app)

@app.route('/')
def index():
    return f'<h1>main page</h1>'

@app.route('/add', methods = ["POST", "GET"])
def add():
    if request.method == "POST":
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        adress = request.form['adress']
        data = (firstname, lastname, adress)
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO clientes (nombre, apellido, direccion) VALUES (%s, %s, %s)', (firstname, lastname, adress))
        mysql.connection.commit()
        return 'succes'
    else:
        return render_template('./form.html')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)